﻿using AutoReservation.Common.Interfaces;
using System;
using System.Diagnostics;
using AutoReservation.Common.DataTransferObjects;
using System.Collections.Generic;
using AutoReservation.BusinessLayer;
using System.ServiceModel;
using System.Linq;

namespace AutoReservation.Service.Wcf
{
    public class AutoReservationService : IAutoReservationService
    {
        private AutoReservationBusinessComponent _businessComponent = new AutoReservationBusinessComponent();

        public List<AutoDto> Autos
        {
            get
            {
                var auto = _businessComponent.GetAutos();
                return auto.ConvertToDtos();
            }
        }

        public List<KundeDto> Kunden
        {
            get
            {
                var kunde = _businessComponent.GetKunden();
                return kunde.ConvertToDtos();
            }
        }

        public List<ReservationDto> Reservationen
        {
            get
            {
                var reservation = _businessComponent.GetReservationen();
                return reservation.ConvertToDtos();
            }
        }

        private static void WriteActualMethod()
        {
            Console.WriteLine($"Calling: {new StackTrace().GetFrame(1).GetMethod().Name}");
        }

        public void DeleteAuto(AutoDto auto)
        {
            _businessComponent.RemoveAuto(auto.ConvertToEntity());
        }

        public void DeleteKunde(KundeDto kunde)
        {
            _businessComponent.RemoveKunde(kunde.ConvertToEntity());
        }

        public void DeleteReservation(ReservationDto reservation)
        {
            _businessComponent.RemoveReservation(reservation.ConvertToEntity());
        }

        public AutoDto GetAutoById(int id)
        {
            return _businessComponent.GetAutoById(id).ConvertToDto();
        }

        public KundeDto GetKundeById(int id)
        {
            return _businessComponent.GetKundeById(id).ConvertToDto();
        }

        public ReservationDto GetReservationByNr(int id)
        {
            return _businessComponent.GetReservationById(id).ConvertToDto();
        }

        public AutoDto InsertAuto(AutoDto auto)
        {
            return _businessComponent.InsertAuto(auto.ConvertToEntity()).ConvertToDto();
        }

        public KundeDto InsertKunde(KundeDto kunde)
        {
            return _businessComponent.InsertKunde(kunde.ConvertToEntity()).ConvertToDto();
        }

        public ReservationDto InsertReservation(ReservationDto reservation)
        {
            return _businessComponent.InsertReservation(reservation.ConvertToEntity()).ConvertToDto();
        }

        public AutoDto UpdateAuto(AutoDto auto)
        {
            return _businessComponent.UpdateAuto(auto.ConvertToEntity()).ConvertToDto();
        }

        public KundeDto UpdateKunde(KundeDto kunde)
        {
            return _businessComponent.UpdateKunde(kunde.ConvertToEntity()).ConvertToDto();
        }

        public ReservationDto UpdateReservation(ReservationDto reservation)
        {
            var cRes = reservation.ConvertToEntity();
            var result = _businessComponent.UpdateReservation(cRes);
            var cbRes = result.ConvertToDto();
            return cbRes;
        }
    }
}