﻿using AutoReservation.Common.Interfaces;
using AutoReservation.Dal;
using AutoReservation.Dal.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using AutoReservation.Common.DataTransferObjects;
using System;
using System.Data.Entity.Migrations;

namespace AutoReservation.BusinessLayer
{
    public class AutoReservationBusinessComponent
    {

        public AutoReservationBusinessComponent()
        {

        }

        private static LocalOptimisticConcurrencyException<T> CreateLocalOptimisticConcurrencyException<T>(
            AutoReservationContext context, T entity)
            where T : class
        {
            var dbEntity = (T) context.Entry(entity)
                .GetDatabaseValues()
                .ToObject();

            return new LocalOptimisticConcurrencyException<T>($"Update {typeof (T).Name}: Concurrency-Fehler", dbEntity);
        }

        public Auto InsertAuto(Auto auto)
        {
            using (var dbContext = new AutoReservationContext())
            {
                var result = dbContext.Autos.Add(auto);
                dbContext.SaveChanges();
                return result;
            }
        }

        public Kunde InsertKunde(Kunde kunde)
        {
            using (var dbContext = new AutoReservationContext())
            {
                var result = dbContext.Kunden.Add(kunde);
                dbContext.SaveChanges();
                return result;
            }
        }

        public Reservation InsertReservation(Reservation reservation)
        {
            using (var dbContext = new AutoReservationContext())
            {
                var result = dbContext.Reservationen.Add(reservation);
                dbContext.SaveChanges();
                return result;
            }
        }

        public Auto GetAutoById(int id)
        {
            using (var dbContext = new AutoReservationContext())
            {
                return dbContext.Autos.FirstOrDefault(a => a.Id == id);
            }
        }

        public IEnumerable<Auto> GetAutos()
        {
            using (var dbContext = new AutoReservationContext())
            {
                return dbContext.Autos.ToList();
            }
        }

        public Kunde GetKundeById(int id)
        {
            using (var dbContext = new AutoReservationContext())
            {
                return dbContext.Kunden.FirstOrDefault(a => a.Id == id);
            }
        }

        public IEnumerable<Kunde> GetKunden()
        {
            using (var dbContext = new AutoReservationContext())
            {
                return dbContext.Kunden.ToList();
            }
        }

        public Reservation GetReservationById(int nr)
        {
            using (var dbContext = new AutoReservationContext())
            {
                return
                    dbContext.Reservationen.Include(r => r.Auto)
                        .Include(r => r.Kunde)
                        .FirstOrDefault(a => a.ReservationsNr == nr);
            }
        }

        public IEnumerable<Reservation> GetReservationen()
        {
            using (var dbContext = new AutoReservationContext())
            {
                return dbContext.Reservationen.Include(r => r.Auto).Include(r => r.Kunde).ToList();
            }
        }

        public void RemoveAuto(Auto auto)
        {
            using (var dbContext = new AutoReservationContext())
            {
                dbContext.Autos.Attach(auto);
                dbContext.Autos.Remove(auto);
                dbContext.SaveChanges();
            }
        }

        public void RemoveKunde(Kunde kunde)
        {
            using (var dbContext = new AutoReservationContext())
            {
                dbContext.Kunden.Attach(kunde);
                dbContext.Kunden.Remove(kunde);
                dbContext.SaveChanges();
            }
        }

        public void RemoveReservation(Reservation reservation)
        {
            using (var dbContext = new AutoReservationContext())
            {
                dbContext.Reservationen.Attach(reservation);
                dbContext.Reservationen.Remove(reservation);
                dbContext.SaveChanges();

            }
        }

        public Auto UpdateAuto(Auto auto)
        {

            using (var dbContext = new AutoReservationContext())
            {
                try
                {
                    dbContext.Entry(auto).State = EntityState.Modified;
                    dbContext.SaveChanges();
                    return auto;
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    throw CreateLocalOptimisticConcurrencyException(dbContext, auto);
                }
            }
        }

        public Kunde UpdateKunde(Kunde kunde)
        {
            using (var dbContext = new AutoReservationContext())
            {
                try
                {
                    dbContext.Entry(kunde).State = EntityState.Modified;
                    dbContext.SaveChanges();
                    return kunde;
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    throw CreateLocalOptimisticConcurrencyException(dbContext, kunde);
                }
            }
        }


        public Reservation UpdateReservation(Reservation reservation)
        {
            using (var dbContext = new AutoReservationContext())
            {
                try
                {
                    reservation.Auto = GetAutoById(reservation.AutoId);
                    reservation.Kunde = GetKundeById(reservation.KundeId);
                    dbContext.Entry(reservation).State = EntityState.Modified;
                    dbContext.SaveChanges();
                    
                    return reservation;
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    throw CreateLocalOptimisticConcurrencyException(dbContext, reservation);
                }
            }
        }
    }
}