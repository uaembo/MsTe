﻿using AutoReservation.Dal.Entities;
using AutoReservation.TestEnvironment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading;

namespace AutoReservation.BusinessLayer.Testing
{
    [TestClass]
    public class BusinessLayerTest
    {

        private AutoReservationBusinessComponent target;
        private AutoReservationBusinessComponent Target
        {
            get
            {
                if (target == null)
                {
                    target = new AutoReservationBusinessComponent();
                }
                return target;
            }
        }
        
        [TestInitialize]
        public void InitializeTestData()
        {
            TestEnvironmentHelper.InitializeTestData();
        }

        [TestMethod]
        public void GetReservationenTest()
        {
            var reservations = Target.GetReservationen();
            Assert.IsNotNull(reservations);
            foreach (var res in reservations)
            {
                Assert.IsNotNull(res.Auto);
                Assert.IsNotNull(res.Kunde);
            }
        }


        [TestMethod]
        public void GetAutoByIdTest()
        {
            Auto auto = Target.GetAutoById(3);
            Assert.AreEqual(3, auto.Id);
            Assert.AreEqual("Audi S6", auto.Marke);
            Assert.AreEqual(180, auto.Tagestarif);
        }

        [TestMethod]
        public void GetKundeByIdTest()
        {
            Kunde kunde = Target.GetKundeById(3);
            Assert.AreEqual(3, kunde.Id);
            Assert.AreEqual("Martha", kunde.Vorname);
            Assert.AreEqual("Pfahl", kunde.Nachname);
            Assert.AreEqual(new DateTime(1990, 7, 3), kunde.Geburtsdatum);
        }

        [TestMethod]
        public void GetReservationByIdTest()
        {
            Reservation reservation = Target.GetReservationById(3);
            Assert.AreEqual(3, reservation.ReservationsNr);
            Assert.AreEqual(new DateTime(2020, 1, 10), reservation.Von);
            Assert.AreEqual(new DateTime(2020, 1, 20), reservation.Bis);
            Assert.AreEqual(3, reservation.Kunde.Id);
            Assert.AreEqual(3, reservation.Auto.Id);
        }

        [TestMethod]
        public void InsertAutoTest()
        {
            Auto auto = new LuxusklasseAuto();
            auto.Tagestarif = 200;
            auto.Marke = "BMW";
            Auto addedAuto = Target.InsertAuto(auto);
            Assert.AreEqual(4, addedAuto.Id);
            Assert.AreEqual(200, addedAuto.Tagestarif);
            Assert.AreEqual("BMW", addedAuto.Marke);
        }

         [TestMethod]

        public void InsertKundeTest()
        {
            Kunde kunde = new Kunde();
            kunde.Vorname = "Michael";
            kunde.Nachname = "Jordan";
            kunde.Geburtsdatum = new DateTime(1990, 01, 08);
            Kunde addedKunde = Target.InsertKunde(kunde);
            Assert.AreEqual(5, addedKunde.Id);
            Assert.AreEqual(new DateTime(1990, 01, 08), addedKunde.Geburtsdatum);
            Assert.AreEqual("Jordan", addedKunde.Nachname);
            Assert.AreEqual("Michael", addedKunde.Vorname);
        }


        [TestMethod]
        public void UpdateAutoTest()
        {
            Auto autoA = Target.GetAutoById(1);
            autoA.Marke = "XYZ";
            Target.UpdateAuto(autoA);
            Auto autoB = Target.GetAutoById(1);
            Assert.AreEqual("XYZ", autoB.Marke);
        }


        [TestMethod]
        [ExpectedException(typeof(LocalOptimisticConcurrencyException<Auto>))]
        public void UpdateAutoTestwithConcurrency()
        {
            Auto autoA = Target.GetAutoById(1);
            autoA.Marke = "XYZ";
            Thread.Sleep(1000);
            Auto autoB = Target.GetAutoById(1);
            autoB.Marke = "ABC";
            Target.UpdateAuto(autoB);
            Target.UpdateAuto(autoA);
        }
        

        [TestMethod]
        public void UpdateKUndeTest()
        {
            Kunde kundeA = Target.GetKundeById(1);
            kundeA.Vorname = "Max";
            Target.UpdateKunde(kundeA);
            Kunde kundeB = Target.GetKundeById(1);
            Assert.AreEqual("Max", kundeB.Vorname);
        }

        [TestMethod]
        [ExpectedException(typeof (LocalOptimisticConcurrencyException<Kunde>))]
        public void UpdateKundeTestwithConcurrency()
        {
            Kunde kundeA = Target.GetKundeById(1);
            kundeA.Vorname = "Max";
            Thread.Sleep(1000);
            Kunde kundeB = Target.GetKundeById(1);
            kundeB.Vorname = "Moritz";
            Target.UpdateKunde(kundeB);
            Target.UpdateKunde(kundeA);
        }

        
        [TestMethod]
        public void UpdateReservationTest()
        {
            Reservation reservationA = Target.GetReservationById(1);
            var date = new DateTime(2016, 12, 09);
            reservationA.Von = date;
            reservationA.Bis = date + TimeSpan.FromDays(1);
            Target.UpdateReservation(reservationA);
            Reservation reservationB = Target.GetReservationById(1);
            Assert.AreEqual(date, reservationB.Von);
            Assert.AreEqual(date + TimeSpan.FromDays(1), reservationB.Bis);
        }

        [TestMethod]
        [ExpectedException(typeof(LocalOptimisticConcurrencyException<Reservation>))]
        public void UpdateReservationTestwithConcurrency()
        {
            Reservation reservationA = Target.GetReservationById(1);
            var date = new DateTime(2016, 12, 09);
            reservationA.Von = date;
            Thread.Sleep(1000);
            Reservation reservationB = Target.GetReservationById(1);
            reservationB.Von = date + TimeSpan.FromDays(1);
            Target.UpdateReservation(reservationB);
            Target.UpdateReservation(reservationA);
        }
    }
}
