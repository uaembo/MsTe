﻿using AutoReservation.Common.DataTransferObjects;
using System.Collections.Generic;
using System.ServiceModel;

namespace AutoReservation.Common.Interfaces
{
    [ServiceContract]
    public interface IAutoReservationService
    {
        List<AutoDto> Autos { [OperationContract]get; }

        [OperationContract]
        AutoDto GetAutoById(int id);

        [OperationContract]
        AutoDto InsertAuto(AutoDto auto);

        [OperationContract]
        AutoDto UpdateAuto(AutoDto auto);

        [OperationContract]
        void DeleteAuto(AutoDto auto);

        List<KundeDto> Kunden { [OperationContract]get; }

        [OperationContract]
        KundeDto GetKundeById(int id);

        [OperationContract]
        KundeDto InsertKunde(KundeDto kunde);

        [OperationContract]
        KundeDto UpdateKunde(KundeDto kunde);

        [OperationContract]
        void DeleteKunde(KundeDto kunde);

        List<ReservationDto> Reservationen { [OperationContract]get; }

        [OperationContract]
        ReservationDto GetReservationByNr(int id);

        [OperationContract]
        ReservationDto InsertReservation(ReservationDto reservation);

        [OperationContract]
        ReservationDto UpdateReservation(ReservationDto reservation);

        [OperationContract]
        void DeleteReservation(ReservationDto reservation);
    }
}
