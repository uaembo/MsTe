﻿using AutoReservation.Common.DataTransferObjects.Core;
using System.Text;

namespace AutoReservation.Common.DataTransferObjects
{
    public class AutoDto : DtoBase<AutoDto>
    {
        private int _id;
        public int Id
        {
            get { return _id; }
            set
            {
                if (_id != value)
                {
                    _id = value;
                    OnPropertyChanged(nameof(Id));
                }
            }
        }

        private string _marke;
        public string Marke
        {
            get { return _marke; }
            set
            {
                if (_marke != value)
                {
                    _marke = value;
                    OnPropertyChanged(nameof(Marke));
                }
            }
        }

        private int _basistarif;
        public int Basistarif
        {
            get { return _basistarif; }
            set
            {
                if (_basistarif != value)
                {
                    _basistarif = value;
                    OnPropertyChanged(nameof(Basistarif));
                }
            }
        }

        private byte[] _rowVersion;
        public byte[] RowVersion
        {
            get { return _rowVersion; }
            set
            {
                if (_rowVersion != value)
                {
                    _rowVersion = value;
                    OnPropertyChanged(nameof(RowVersion));
                }
            }
        }


        private int _tagestarif;
        public int Tagestarif
        {
            get { return _tagestarif; }
            set
            {
                if (_tagestarif != value)
                {
                    _tagestarif = value;
                    OnPropertyChanged(nameof(Tagestarif));
                }
            }
        }

        private AutoKlasse _autoKlasse;
        public AutoKlasse AutoKlasse
        {
            get { return _autoKlasse; }
            set
            {
                if (_autoKlasse != value)
                {
                    _autoKlasse = value;
                    OnPropertyChanged(nameof(AutoKlasse));
                }
            }
        }

        public override string Validate()
        {
            StringBuilder error = new StringBuilder();
            if (string.IsNullOrEmpty(Marke))
            {
                error.AppendLine("- Marke ist nicht gesetzt.");
            }
            if (Tagestarif <= 0)
            {
                error.AppendLine("- Tagestarif muss grösser als 0 sein.");
            }
            if (AutoKlasse == AutoKlasse.Luxusklasse && Basistarif <= 0)
            {
                error.AppendLine("- Basistarif eines Luxusautos muss grösser als 0 sein.");
            }

            if (error.Length == 0) { return null; }

            return error.ToString();
        }

        public override string ToString()
            => $"{Id}; {Marke}; {Tagestarif}; {Basistarif}; {AutoKlasse}";

    }
}
