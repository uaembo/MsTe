﻿using AutoReservation.Common.DataTransferObjects.Core;
using System;
using System.Text;

namespace AutoReservation.Common.DataTransferObjects
{
    public class ReservationDto : DtoBase<ReservationDto>
    {
        private DateTime _von;
        public DateTime Von
        {
            get { return _von; }
            set
            {
                if (_von != value)
                {
                    _von = value;
                    OnPropertyChanged(nameof(Von));
                }
            }
        }

        private DateTime _bis;
        public DateTime Bis
        {
            get { return _bis; }
            set
            {
                if (_bis != value)
                {
                    _bis = value;
                    OnPropertyChanged(nameof(Bis));
                }
            }
        }

        private AutoDto _auto;
        public AutoDto Auto
        {
            get { return _auto; }
            set
            {
                if (_auto != value)
                {
                    _auto = value;
                    OnPropertyChanged(nameof(Auto));
                }
            }
        }

        private KundeDto _kunde;
        public KundeDto Kunde
        {
            get { return _kunde; }
            set
            {
                if (_kunde != value)
                {
                    _kunde = value;
                    OnPropertyChanged(nameof(Kunde));
                }
            }
        }

        private int _reservationNr;
        public int ReservationsNr 
        {
            get { return _reservationNr; }
            set
            {
                if (_reservationNr != value)
                {
                    _reservationNr = value;
                    OnPropertyChanged(nameof(ReservationsNr));
                }
            }
        }

        private byte[] _rowVersion;
        public byte[] RowVersion
        {
            get { return _rowVersion; }
            set
            {
                if (_rowVersion != value)
                {
                    _rowVersion = value;
                    OnPropertyChanged(nameof(RowVersion));
                }
            }
        }


        public override string Validate()
        {
            StringBuilder error = new StringBuilder();
            if (Von == DateTime.MinValue)
            {
                error.AppendLine("- Von-Datum ist nicht gesetzt.");
            }
            if (Bis == DateTime.MinValue)
            {
                error.AppendLine("- Bis-Datum ist nicht gesetzt.");
            }
            if (Von > Bis)
            {
                error.AppendLine("- Von-Datum ist grösser als Bis-Datum.");
            }
            if (Auto == null)
            {
                error.AppendLine("- Auto ist nicht zugewiesen.");
            }
            else
            {
                string autoError = Auto.Validate();
                if (!string.IsNullOrEmpty(autoError))
                {
                    error.AppendLine(autoError);
                }
            }
            if (Kunde == null)
            {
                error.AppendLine("- Kunde ist nicht zugewiesen.");
            }
            else
            {
                string kundeError = Kunde.Validate();
                if (!string.IsNullOrEmpty(kundeError))
                {
                    error.AppendLine(kundeError);
                }
            }

            if (error.Length == 0) { return null; }

            return error.ToString();
        }

        public override string ToString()
            => $"{ReservationsNr}; {Von}; {Bis}; {Auto}; {Kunde}";

    }
}
