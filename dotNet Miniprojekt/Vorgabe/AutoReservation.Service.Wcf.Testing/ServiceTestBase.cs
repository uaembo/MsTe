﻿using AutoReservation.BusinessLayer;
using AutoReservation.Common.DataTransferObjects;
using AutoReservation.Common.Interfaces;
using AutoReservation.Dal.Entities;
using AutoReservation.TestEnvironment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.ServiceModel;

namespace AutoReservation.Service.Wcf.Testing
{
    [TestClass]
    public abstract class ServiceTestBase
    {
        protected abstract IAutoReservationService Target { get; }

        [TestInitialize]
        public void InitializeTestData()
        {
            TestEnvironmentHelper.InitializeTestData();
        }

        #region Read all entities

        [TestMethod]
        public void GetAutosTest()
        {
            Assert.AreEqual(3, Target.Autos.Count);
            Assert.AreEqual("Fiat Punto", Target.Autos[0].Marke);   
        }

        [TestMethod]
        public void GetKundenTest()
         {
            Assert.AreEqual(4, Target.Kunden.Count);
            Assert.AreEqual("Anna", Target.Kunden[0].Vorname);
        }

        [TestMethod]
        public void GetReservationenTest()
        {
            Assert.AreEqual(3, Target.Reservationen.Count);
            Assert.AreEqual("Fiat Punto", Target.Reservationen[0].Auto.Marke);
            Assert.AreEqual("Beil", Target.Reservationen[1].Kunde.Nachname);
        }

        #endregion

        #region Get by existing ID

        [TestMethod]
        public void GetAutoByIdTest()
        {
            Assert.AreEqual(Target.GetAutoById(1).Id, 1);
        }

        [TestMethod]
        public void GetKundeByIdTest()
        {
            Assert.AreEqual(Target.GetKundeById(1).Id, 1);
        }

        [TestMethod]
        public void GetReservationByNrTest()
        {
            Assert.AreEqual(Target.GetReservationByNr(1).ReservationsNr, 1);
        }

        #endregion

        #region Get by not existing ID

        [TestMethod]
        public void GetAutoByIdWithIllegalIdTest()
        {
            Assert.AreEqual(null, Target.GetAutoById(-1));
        }

        [TestMethod]
        public void GetKundeByIdWithIllegalIdTest()
        {
            Assert.AreEqual(null, Target.GetKundeById(-1));
        }

        [TestMethod]
        public void GetReservationByNrWithIllegalIdTest()
        {
            Assert.AreEqual(null, Target.GetReservationByNr(-1));
        }

        #endregion

        #region Insert

        [TestMethod]
        public void InsertAutoTest()
        {
            var auto = new AutoDto() { Marke = "BMW" };
            var insertedAuto = Target.InsertAuto(auto);
            Assert.IsNotNull(insertedAuto);
        }

        [TestMethod]
        public void InsertKundeTest()
        {
            var kunde = new KundeDto() { Nachname = "Meier", Geburtsdatum = DateTime.Now };
            var insertedKunde = Target.InsertKunde(kunde);
            Assert.IsNotNull(insertedKunde);
        }

        [TestMethod]
        public void InsertReservationTest()
        {
            var auto = Target.GetAutoById(1);
            var kunde = Target.GetKundeById(1);
            var reservation = new ReservationDto()
            {
                Auto = auto,
                Kunde = kunde,
                Von = DateTime.Now,
                Bis = DateTime.Now + TimeSpan.FromHours(10)
            };

            var insertedReservation = Target.InsertReservation(reservation);
            Assert.IsNotNull(insertedReservation);
        }

        #endregion

        #region Delete  

        [TestMethod]
        public void DeleteAutoTest()
        {
            var audiS6 = Target.GetAutoById(3);
            Target.DeleteAuto(audiS6);

            Assert.AreEqual(null, Target.GetAutoById(3));
        }

        [TestMethod]
        public void DeleteKundeTest()
        {
            var frauPfahl = Target.GetKundeById(3);
            Target.DeleteKunde(frauPfahl);

            Assert.AreEqual(null, Target.GetKundeById(3));
        }

        [TestMethod]
        public void DeleteReservationTest()
        {
            var reservation = Target.GetReservationByNr(3);
            Target.DeleteReservation(reservation);

            Assert.AreEqual(null, Target.GetReservationByNr(3));
        }

        #endregion

        #region Update

        [TestMethod]
        public void UpdateAutoTest()
        {
            var auto = Target.GetAutoById(1);
            var oldMarke = auto.Marke;
            var newMarke = "BMW";
            auto.Marke = newMarke;

            var updatedAuto = Target.UpdateAuto(auto);
            Assert.AreEqual(newMarke, updatedAuto.Marke);
        }

        [TestMethod]
        public void UpdateKundeTest()
        {
            var kunde = Target.GetKundeById(1);
            var oldVorname = kunde.Vorname;
            var newVorname = "Michael";
            kunde.Vorname = newVorname;

            var updatedKunde= Target.UpdateKunde(kunde);
            Assert.AreEqual(newVorname, updatedKunde.Vorname);
        }

        [TestMethod]
        public void UpdateReservationTest()
        {
            var reservation = Target.GetReservationByNr(1);
            var oldAuto = reservation.Auto;

            var newAuto = Target.GetAutoById(2);
            reservation.Auto = newAuto;

            var updatedReservation = Target.UpdateReservation(reservation);
            Assert.AreEqual(newAuto.Marke, updatedReservation.Auto.Marke);
        }

        #endregion

        #region Update with optimistic concurrency violation

        [TestMethod]
        [ExpectedException(typeof(LocalOptimisticConcurrencyException<Auto>))]
        public virtual void UpdateAutoWithOptimisticConcurrencyTest()
        {
            //client 1
            var auto = Target.GetAutoById(1);
            var oldMarke = auto.Marke;
            var newMarke = "BMW";
            auto.Marke = newMarke;

            //client 2
            var auto2 = Target.GetAutoById(1);
            var oldMarke2 = auto2.Marke;
            var newMarke2 = "Mercedes";
            auto2.Marke = newMarke2;

            //client 2 commited zuerst
            var updatedAuto2 = Target.UpdateAuto(auto2);
            //expecting exception here
            var updatedAuto = Target.UpdateAuto(auto);
        }

        [TestMethod]
        [ExpectedException(typeof(LocalOptimisticConcurrencyException<Kunde>))]
        public virtual void UpdateKundeWithOptimisticConcurrencyTest()
        {
            //client 1
            var kunde = Target.GetKundeById(1);
            var oldName = kunde.Nachname;
            var newName = "Stocker";
            kunde.Nachname = newName;

            //client 2
            var kunde2 = Target.GetKundeById(1);
            var oldName2 = kunde2.Nachname;
            var newName2 = "Sommer";
            kunde2.Nachname = newName2;

            //client 2 commited zuerst
            var updatedKunde2 = Target.UpdateKunde(kunde2);

            //expecting exception here
            var updatedKunde = Target.UpdateKunde(kunde);

        }

        [TestMethod]
        [ExpectedException(typeof(LocalOptimisticConcurrencyException<Reservation>))]
        public virtual void UpdateReservationWithOptimisticConcurrencyTest()
        {
            //client 1
            var reservation = Target.GetReservationByNr(1);
            var oldResBis = reservation.Bis;
            var newResBis = oldResBis + TimeSpan.FromHours(1);
            reservation.Bis = newResBis;

            //client 2
            var reservation2 = Target.GetReservationByNr(1);
            var oldResBis2 = reservation2.Bis;
            var newResBis2 = oldResBis2 + TimeSpan.FromHours(2);
            reservation2.Bis = newResBis2;

            //client 2 commited zuerst
            var updatedRes2 = Target.UpdateReservation(reservation2);
            //expecting exception here
            var updatedRes = Target.UpdateReservation(reservation);
        }

        #endregion
    }
}
