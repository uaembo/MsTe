using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace AutoReservation.Dal.Entities
{
    public class Auto
    {
        public int Id { get; set; }

        public string Marke{ get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        public int Tagestarif { get; set; }
        
        public virtual ICollection<Reservation> Reservationen { get; set; } = new HashSet<Reservation>();
    }
}
