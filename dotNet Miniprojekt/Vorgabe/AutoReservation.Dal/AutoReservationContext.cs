﻿using AutoReservation.Dal.Entities;
using AutoReservation.Dal.Migrations;
using System.Data.Entity;

namespace AutoReservation.Dal
{
    public class AutoReservationContext : DbContext
    {
        public virtual DbSet<Auto> Autos { get; set; }
        public virtual DbSet<Kunde> Kunden { get; set; }
        public virtual DbSet<Reservation> Reservationen { get; set; }

        public AutoReservationContext()
        {
            // Ensures that the database will be initialized
            Database.Initialize(false);

            // Disable lazy loading
            Configuration.LazyLoadingEnabled = false;

            // ----------------------------------------------------------------------------------------------------
            // Choose one of these three options:

            // Use for real "database first"
            //      - Database will NOT be created by Entity Framework
            //      - Database will NOT be modified by Entity Framework
            // Database.SetInitializer<AutoReservationContext>(null);

            // Use this for initial "code first" 
            //      - Database will be created by Entity Framework
            //      - Database will NOT be modified by Entity Framework
            // Database.SetInitializer(new CreateDatabaseIfNotExists<AutoReservationContext>());

            // Use this for real "code first" 
            //      - Database will be created by Entity Framework
            //      - Database will be modified by Entity Framework
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AutoReservationContext, Configuration>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Auto>()
                .Map<LuxusklasseAuto>(c => c.Requires("Discriminator").HasValue("LuxusklasseAuto"))
                .Map<MittelklasseAuto>(c => c.Requires("Discriminator").HasValue("MittelklasseAuto"))
                .Map<StandardAuto>(c => c.Requires("Discriminator").HasValue("StandardAuto"))
                .ToTable("Autoes");

            modelBuilder.Entity<Auto>()
                .Property(t => t.RowVersion)
                .IsRowVersion();

            modelBuilder.Entity<Kunde>()
               .Property(t => t.RowVersion)
               .IsRowVersion();

            modelBuilder.Entity<Reservation>()
               .Property(t => t.RowVersion)
               .IsRowVersion();
            
        }
    }
}
